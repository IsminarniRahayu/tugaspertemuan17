package main

import (
	"be04gomy/handler"
	"be04gomy/handler/hGuest"
)

const PORT = `:8000`
func main() {
	server := handler.InitServer(`views/`)
	server.Handle(`/guest/antrian/list`,hGuest.AntrianList)
	server.Handle(`/guest/antrian/create`,hGuest.AntrianCreate)
	server.Handle(`/guest/antrian/update`,hGuest.AntrianUpdate)
	server.Handle(`/guest/antrian/delete`,hGuest.AntrianDelete)
	server.Listen(PORT) 
}
