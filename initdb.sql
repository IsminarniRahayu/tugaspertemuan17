CREATE DATABASE tugass;
USE tugass;
CREATE USER `user3`@`localhost` IDENTIFIED BY '1234567';
GRANT ALL PRIVILEGES ON tugass.* TO `user3`@`localhost`;
FLUSH PRIVILEGES; 
-- mysql -u user3 -p tugass

CREATE TABLE antriann (
	`id` INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
	`nomor` VARCHAR(12) NOT NULL,
	`status` VARCHAR(64) NOT NULL,
	`pelanggan` VARCHAR(64) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL
);
