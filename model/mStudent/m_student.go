package mStudent

import (
	
	"database/sql"
	"time"
	"fmt"
)

type Antrian struct {
	Id int64 
	Nomor string // `json:"nim"`
	Status string
	Pelanggan string
	CreatedAt time.Time
	UpdatedAt time.Time
}

const dateFormat = `2006-01-02 15:04:05`

func SelectAll(db *sql.DB) (antriann []Antrian, err error) {
	rows, err := db.Query(`SELECT * FROM antriann ORDER BY id DESC`)
	antriann = []Antrian{}
	if err != nil {
		return antriann, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Antrian{}
		createdAt, updatedAt := ``, ``
		err = rows.Scan(
			&s.Id,
			&s.Nomor,
			&s.Status,
			&s.Pelanggan,
			&createdAt,
			&updatedAt)
		if err != nil {
			return 
		}
		s.CreatedAt, _ = time.Parse(dateFormat, createdAt)
		s.UpdatedAt, _ = time.Parse(dateFormat, updatedAt)
		antriann = append(antriann, s)
	}
	return
}

func Insert(db *sql.DB, m *Antrian) (err error) {
	now := time.Now()
	res, err := db.Exec(`INSERT INTO antriann(nomor,status,pelanggan,created_at,updated_at)
VALUES(?,?,?,?,?)`,
	m.Nomor,
	m.Status,
	m.Pelanggan,
	now,
	now)
	if err !=nil {
		return err
	}
	m.Id, err = res.LastInsertId()
	if err == nil {
		m.CreatedAt = now
		m.UpdatedAt = now
	}
	return err
}

func Update(db *sql.DB, m *Antrian) (affectedRows int64, err error){
	s:= Antrian{}
	rows, err := db.Query(`SELECT * FROM antriann WHERE id=?`, m.Id)
	if err != nil {return}
	defer rows.Close()
	for rows.Next(){
		createdAt, UpdatedAt := ``, ``
		err = rows.Scan(
			&s.Id,
			&s.Nomor,
			&s.Status,
			&s.Pelanggan,
			&createdAt,
			&UpdatedAt)
			if err != nil {return}
	}
		if m.Nomor == "" {m.Nomor = s.Nomor}
		if m.Status == "" {m.Status = s.Status}
		if m.Pelanggan == "" {m.Pelanggan = s.Pelanggan}
		now := time.Now()
		res, err := db.Exec(`UPDATE antriann SET nomor=?, status=?, pelanggan=?, updated_at=? WHERE id=?`,
			m.Nomor,
			m.Status,
			m.Pelanggan,
			now,
			m.Id)
		if err != nil {
			return
		}
		return res.RowsAffected()
	}
	//isminarni Rahayu

	func Delete(db *sql.DB, m *Antrian) (deletedRecord Antrian, success bool) {
		s:= Antrian{}
		rows, err := db.Query(`SELECT * FROM antriann WHERE id=?`, m.Id)
		if err != nil{
			return s, false
		}
		defer rows.Close()
		for rows.Next(){
			createdAt, updatedAt := ``,``
			err = rows.Scan(
				&s.Id,
				&s.Nomor,
				&s.Status,
				&s.Pelanggan,
				&createdAt,
				&updatedAt)
			if err != nil{
				return
			}
			s.CreatedAt, _ = time.Parse(dateFormat, createdAt)
			s.UpdatedAt, _ = time.Parse(dateFormat, updatedAt)
		}
		res, err := db.Exec(`DELETE FROM antriann WHERE id=?`, m.Id)
		if err != nil{
			return
		}
		fmt.Printf("%s", res)
		return s, true
	}
	